Warcaby wersja beta1
Oddaję sprawną (w miarę możliwości) wersję gry warcaby.
Warto powiedzieć na wstępie co działa i co zostało sprawdzone:
1. program działa stabilnie - cokolwiek robimy.
2. pola do wpisywania danych mają walidację, która chroni przed "głupimi blondynkami"
3. pionki mogą wykonywać ruchy według zasad:
	I, pola po skosie do przodu. o 1 pole.
	II. bicia do przodu i do tyłu.
	(w przypadku wykrycia możliwości bicia można wykonać dodatkowy ruch)
4. gra się kończy.

A teraz najważniejsze - co nie działa?
1. BRAK DAMEK 
2. automatyczne bicie.
3. "za nie bicie, tracisz życie"
4. algorytm MIN_MAX. gra z komputerem.
(nic innego nie przychodzi mi do głowy)

Lesyek
